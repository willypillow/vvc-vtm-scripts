#!/usr/bin/env bash
for i in $(seq 1 3); do
	for j in 22 27 32 37; do
		grep -A 1 'Total Frames' $1/$2-$i-$j.log | tail -n 1 | awk '{ printf "%s", $3 "," $4 "," $5 "," $6 ","; }'
		grep 'Total Time' $1/$2-$i-$j.log | awk '{printf "%s,", $3;}'
		grep 'Total Time' $1/$2-$i-$j.bin.decodelog | awk '{print $3;}'
	done
done
