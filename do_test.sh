#!/usr/bin/env bash

if [ $# -ne 2 ]; then
  echo "Usage: $0 [result_dir] [JVET]"
  exit 1
fi

ODIR="$(pwd)"
NDIR="$1/$2"

set -e

for i in AHG14 O0549 O0625 P0042 P0043 P0057 P0058 P0059 P0063 P0072 P0077 P0081 P0088 P0090 P0091 P0092 P01034 P0111 P0152 P0154 P0158 P0162 P0164 P0170 P0199 P0206 P0273 P0298 P0325 P0329 P0335 P0345 P0347 P0350 P0371 P0385 P0400 P0406 P0418 P0436 P0445 P0460 P0469 P0491 P0505 P0512 P0516 P0526 P0530 P0562 P0571 P0578 P0599 P0615 P0641 P0653 P0667 P0803 P0983 P1000 P1001 P1018 P1023 P1026; do
  sed -i 's/^\(#define JVET_'$i'_[^ ]*  *\)0/\11/g' source/Lib/CommonLib/TypeDef.h;
done
sed -i 's/^\(#define JVET_'$2'_[^ ]*  *\)1/\10/g' source/Lib/CommonLib/TypeDef.h;

rm -rf build
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j24
cd "$ODIR"

mkdir -p "$NDIR"

cd "$ODIR"
for qp in 22 27; do
  i=1
  for file in "$ODIR"/../yuv-data/*.yuv; do
    cd "$ODIR"
    mkdir "$NDIR"/{ra,intra,ld,ldp}-$i-$qp
    cd "$NDIR"/ra-$i-$qp

    \time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
        -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_randomaccess_vtm.cfg \
        >> ../ra-$i-$qp.log 2>> ../ra-$i-$qp.log &
    cd ../intra-$i-$qp
    \time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
        -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_intra_vtm.cfg \
        >> ../intra-$i-$qp.log 2>> ../intra-$i-$qp.log &
    #cd ../ld-$i-$qp
    #\time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
    #    -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_lowdelay_vtm.cfg \
    #    >> ../ld-$i-$qp.log 2>> ../ld-$i-$qp.log &
    #cd ../ldp-$i-$qp
    #\time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
    #    -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_lowdelay_P_vtm.cfg \
    #    >> ../ldp-$i-$qp.log 2>> ../ldp-$i-$qp.log &
    cd ../..
    i=$((i+1))
  done
done

wait $(jobs -p)
cd "$ODIR"

for ((k=1;k<$i;k++)); do
  for qp in 22 27; do
    for j in ra intra ld ldp; do
      mv "$NDIR"/$j-$k-$qp/*.yuv "$NDIR"/$j-$k-$qp.yuv
      mv "$NDIR"/$j-$k-$qp/*.bin "$NDIR"/$j-$k-$qp.bin
      rm -rf "$NDIR"/$j-$k-$qp
    done
  done
done

cd "$ODIR"
for qp in 32 37; do
  i=1
  for file in "$ODIR"/../yuv-data/*.yuv; do
    cd "$ODIR"
    mkdir "$NDIR"/{ra,intra,ld,ldp}-$i-$qp
    cd "$NDIR"/ra-$i-$qp

    \time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
        -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_randomaccess_vtm.cfg \
        >> ../ra-$i-$qp.log 2>> ../ra-$i-$qp.log &
    cd ../intra-$i-$qp
    \time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
        -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_intra_vtm.cfg \
        >> ../intra-$i-$qp.log 2>> ../intra-$i-$qp.log &
    #cd ../ld-$i-$qp
    #\time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
    #    -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_lowdelay_vtm.cfg \
    #    >> ../ld-$i-$qp.log 2>> ../ld-$i-$qp.log &
    #cd ../ldp-$i-$qp
    #\time "$ODIR"/bin/EncoderAppStatic -i $file -wdt 320 -hgt 240 -fr 30 \
    #    -f 30 --InputBitDepth=10 --QP=$qp -c "$ODIR"/cfg/encoder_lowdelay_P_vtm.cfg \
    #    >> ../ldp-$i-$qp.log 2>> ../ldp-$i-$qp.log &
    cd ../..
    i=$((i+1))
  done
done

wait $(jobs -p)
cd "$ODIR"

for ((k=1;k<$i;k++)); do
  for qp in 32 37; do
    for j in ra intra ld ldp; do
      mv "$NDIR"/$j-$k-$qp/*.yuv "$NDIR"/$j-$k-$qp.yuv
      mv "$NDIR"/$j-$k-$qp/*.bin "$NDIR"/$j-$k-$qp.bin
      rm -rf "$NDIR"/$j-$k-$qp
    done
  done
done
