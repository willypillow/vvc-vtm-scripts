#!/usr/bin/env bash

set -e

sed -ie 's/^\(#define JVET_'$1'_.*\) 1/\1 0/' source/Lib/CommonLib/TypeDef.h

rm -rf build
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j24
cd ..

result="result$1"

mkdir -p $result
for qp in 22 27 32 37; do
	i=1
	for file in ../yuv-data/*.yuv; do
		mkdir $result/{ra,intra}-$i-$qp
		cd $result/ra-$i-$qp
		\time ../../bin/EncoderAppStatic -i ../../$file -wdt 320 -hgt 240 -fr 30 \
				-f 30 --InputBitDepth=10 --QP=$qp -c ../../cfg/encoder_randomaccess_vtm.cfg \
				>> ../ra-$i-$qp.log 2>> ../ra-$i-$qp.log &
		cd ../intra-$i-$qp
		\time ../../bin/EncoderAppStatic -i ../../$file -wdt 320 -hgt 240 -fr 30 \
				-f 30 --InputBitDepth=10 --QP=$qp -c ../../cfg/encoder_intra_vtm.cfg \
				>> ../intra-$i-$qp.log 2>> ../intra-$i-$qp.log &
		#cd ../ld-$i-$qp
		#\time ../../bin/EncoderAppStatic -i ../../$file -wdt 320 -hgt 240 -fr 30 \
		#		-f 30 --InputBitDepth=10 --QP=$qp -c ../../cfg/encoder_lowdelay_vtm.cfg \
		#		>> ../ld-$i-$qp.log 2>> ../ld-$i-$qp.log &
		#cd ../ldp-$i-$qp
		#\time ../../bin/EncoderAppStatic -i ../../$file -wdt 320 -hgt 240 -fr 30 \
		#		-f 30 --InputBitDepth=10 --QP=$qp -c ../../cfg/encoder_lowdelay_P_vtm.cfg \
		#		>> ../ldp-$i-$qp.log 2>> ../ldp-$i-$qp.log &
		cd ../..
		i=$((i+1))
	done
done

wait $(jobs -p)

for ((k=1;k<$i;k++)); do
	for qp in 22 27 32 37; do
		for j in ra intra; do
			mv $result/$j-$k-$qp/*.yuv $result/$j-$k-$qp.yuv
			mv $result/$j-$k-$qp/*.bin $result/$j-$k-$qp.bin
			rm -rf $result/$j-$k-$qp
		done
	done
done

for file in $result/*.bin; do
	\time bin/DecoderAppStatic -b $file -dph 0 > $file.decodelog 2>&1 &
	i=$((i+1))
done

wait $(jobs -p)
