A set of scripts to test JVET VVC proposals according to *JVET-P0013*.

A lot of options are currently hardcoded, though they should be relatively easy to modify.

See the source code for usage.

- `do_test.sh`: Runs the encoding process.
- `do_test_dec.sh`: Runs the decoding process.
- `parse-and-create-xlsm.sh`: Parses resulting log files and writes corresponding XLSM files. (Requires [Ammonite](https://ammonite.io/) to run.)
