#!/usr/bin/env amm
import $ivy.`org.apache.poi:poi:4.1.1`
import $ivy.`org.apache.poi:poi-ooxml:4.1.1`
import $ivy.`org.apache.poi:poi-ooxml-schemas:4.1.1`

import scala.collection.JavaConverters._
import scala.io.Source
import org.apache.poi.ss.usermodel._

val Reference = "VTM-7.0"
val CpuModel = "Intel Xeon E5-2620 v4"
val Hyperthreading = "on"
val TurboBoost = "on"
val Compiler = "GCC 9.2.0"
val Os = "Arch Linux 2020-01"
val BitstreamConcat = "no"
val Md5Sei = "no"
val YuvOutput = "no"
val SimdOptions = "AVX2"

def pasteFromCsv(csvPath: os.Path, sheet: Sheet, offset: Int) = {
	val csv = Source.fromFile(csvPath.toString).getLines.map(_.split(",").map(_.trim))
	csv.zip(sheet.asScala.drop(offset)).foreach {
		case (line, row) =>
			line.zipWithIndex.foreach {
				case (dat, j) =>
					val cell = row.createCell(j + 1)
					cell.setCellValue(dat.toDouble)
					cell.setCellType(CellType.NUMERIC)
			}
	}
}

@main
def main(templatePath: os.Path, refPath: os.Path, testPath: os.Path, outPath: os.Path, offset: Int, testName: String) = {
	val wb = WorkbookFactory.create(new java.io.File(templatePath.toString))
	val mainSh = wb.getSheet("Summary")
	mainSh.getRow(2).getCell(9).setCellValue(Reference)
	mainSh.getRow(3).getCell(9).setCellValue(testName)
	mainSh.getRow(14).getCell(9).setCellValue(CpuModel)
	mainSh.getRow(16).getCell(9).setCellValue(Hyperthreading)
	mainSh.getRow(17).getCell(9).setCellValue(TurboBoost)
	mainSh.getRow(19).getCell(9).setCellValue(Compiler)
	mainSh.getRow(21).getCell(9).setCellValue(Os)
	mainSh.getRow(25).getCell(9).setCellValue(BitstreamConcat)
	mainSh.getRow(27).getCell(9).setCellValue(Md5Sei)
	mainSh.getRow(28).getCell(9).setCellValue(YuvOutput)
	mainSh.getRow(30).getCell(9).setCellValue(SimdOptions)
	pasteFromCsv(refPath, wb.getSheet("Reference"), offset)
	pasteFromCsv(testPath, wb.getSheet("Test"), offset)
	wb.setForceFormulaRecalculation(true)
	wb.write(new java.io.FileOutputStream(outPath.toString))
}
