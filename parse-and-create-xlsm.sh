#!/usr/bin/env bash
./parse-to-csv.sh result_all/$1 intra > tmp.csv
./csv-to-xlsm.sc template.xlsm reference-intra.csv tmp.csv $1.xlsm 0 "$1 off"

mv $1.xlsm tmp.xlsm
./parse-to-csv.sh result_all/$1 ra > tmp.csv
./csv-to-xlsm.sc tmp.xlsm reference-ra.csv tmp.csv $1.xlsm 104 "$1 off"

#mv $1.xlsm tmp.xlsm
#./parse-to-csv.sh result_all/$1 ld > tmp.csv
#./csv-to-xlsm.sc tmp.xlsm reference-ld.csv tmp.csv $1.xlsm 208 $1
#
#mv $1.xlsm tmp.xlsm
#./parse-to-csv.sh result_all/$1 ldp > tmp.csv
#./csv-to-xlsm.sc tmp.xlsm reference-ldp.csv tmp.csv $1.xlsm 312 $1
